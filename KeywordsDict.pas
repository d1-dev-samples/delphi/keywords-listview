unit KeywordsDict;

interface
uses
  Vcl.Graphics,
  System.Classes,
  Generics.Collections;

type
  TKeywordList = class
  private
    FName: string;
    FKeywords: TStringList;
    FColor: TColor;
  public
    constructor Create(Name: string; Color: TColor);
    destructor Destroy; override;
    property Name: string read FName;
    property Keywords: TStringList read FKeywords;
    property Color: TColor read FColor write FColor;
  end;


  TKeywordsDictionary = class(TComponent)
  private
    FDict: TDictionary<string, TKeywordList>;

  public
    constructor Create(AOwner: TComponent);
    destructor Destroy; override;

    procedure Clear;

    function ContainsList(ListName: string): boolean;
    function ContainsKeyword(ListName: string; Keyword: string): boolean;

    function AddList(ListName: string; Color: TColor): boolean;
    function AddKeyword(ListName: string; Keyword: string): boolean;

    function RemoveList(ListName: string): boolean;
    function RemoveKeyword(ListName: string; Keyword: string): boolean;

    function GetLists: TArray<TKeywordList>;
    function GetKeywords(ListName: string): TStringList;

    function SetListColor(ListName: string; Color: TColor): boolean;
    function GetListColor(ListName: string): TColor;
  end;

implementation

{ TKeywordList }

constructor TKeywordList.Create(Name: string; Color: TColor);
begin
  FName := Name;
  FColor := Color;
  FKeywords := TStringList.Create;
  FKeywords.Sorted := true;
  FKeywords.Duplicates := dupError;
end;

destructor TKeywordList.Destroy;
begin
  FKeywords.Free;
end;


{ TKeywordsDictionary }

constructor TKeywordsDictionary.Create(AOwner: TComponent);
begin
  if AOwner <> nil then
    AOwner.InsertComponent(self);

  FDict := TDictionary<string, TKeywordList>.Create;
end;

destructor TKeywordsDictionary.Destroy;
begin
  Clear;
  FDict.Free;
end;

procedure TKeywordsDictionary.Clear;
begin
  for var sl in FDict.Values do
    sl.free;
  FDict.Clear;
end;

function TKeywordsDictionary.AddKeyword(ListName, Keyword: string): boolean;
var
  LList: TKeywordList;
  i: integer;
begin
  result := false;
  if (FDict.TryGetValue(ListName, LList)) and (LList <> nil) and (not LList.Keywords.Find(Keyword, i)) then
  begin
    LList.Keywords.Add(Keyword);
    result := true;
  end;
end;

function TKeywordsDictionary.AddList(ListName: string; Color: TColor): boolean;
var
  LList: TKeywordList;
begin
  result := false;
  if ContainsList(ListName) then
    exit;

  try
    LList := TKeywordList.Create(ListName, Color);
    FDict.Add(ListName, LList);
    result := true;
  except
    LList.Free;
  end;
end;

function TKeywordsDictionary.ContainsKeyword(ListName, Keyword: string): boolean;
var
  LList: TKeywordList;
  i: integer;
begin
  result := (FDict.TryGetValue(ListName, LList)) and (LList <> nil) and (LList.Keywords.Find(Keyword, i));
end;

function TKeywordsDictionary.ContainsList(ListName: string): boolean;
begin
  result := FDict.ContainsKey(ListName);
end;

function TKeywordsDictionary.RemoveKeyword(ListName, Keyword: string): boolean;
var
  LList: TKeywordList;
  i: integer;
begin
  result := false;
  if (FDict.TryGetValue(ListName, LList)) and (LList <> nil) and (LList.Keywords.Find(Keyword, i)) then
  begin
    LList.Keywords.Delete(i);
    result := true;
  end;
end;

function TKeywordsDictionary.RemoveList(ListName: string): boolean;
var
  LList: TKeywordList;
begin
  result := false;
  if FDict.TryGetValue(ListName, LList) then
  begin
    LList.Free;
    FDict.Remove(ListName);
    FDict.TrimExcess;
    result := true;
  end;
end;

function TKeywordsDictionary.SetListColor(ListName: string; Color: TColor): boolean;
begin
  result := false;
  if FDict.ContainsKey(ListName) then
  begin
    FDict.Items[ListName].Color := Color;
    result := true;
  end;
end;

function TKeywordsDictionary.GetListColor(ListName: string): TColor;
begin
  result := clBlack;
  if FDict.ContainsKey(ListName) then
    result := FDict.Items[ListName].Color;
end;

function TKeywordsDictionary.GetLists: TArray<TKeywordList>;
begin
  result := FDict.Values.ToArray;
end;

function TKeywordsDictionary.GetKeywords(ListName: string): TStringList;
begin
  result := nil;
  if FDict.ContainsKey(ListName) then
    result := FDict.Items[ListName].Keywords;
end;

end.
